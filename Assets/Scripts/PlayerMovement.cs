﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

    public float maxSpeed = 3;
    public float speed = 50f;
    public float jumpSpeed = 5;

    public int JUMP = 2;

    private Rigidbody2D rb2d;
    

	void Start () {
        rb2d = gameObject.GetComponent<Rigidbody2D>(); 
	}
	

	void Update () {
        float h = Input.GetAxis("Horizontal");
        rb2d.AddForce((Vector2.right * speed) * h);

        if(rb2d.velocity.x > maxSpeed)
        {
            rb2d.velocity = new Vector2(maxSpeed, rb2d.velocity.y);
        }
        if(rb2d.velocity.x < -maxSpeed)
        {
            rb2d.velocity = new Vector2(-maxSpeed, rb2d.velocity.y);
        }

        if(Input.GetButtonDown("Jump") && JUMP == 2 )
        {
            rb2d.velocity = new Vector2(rb2d.velocity.x, jumpSpeed);
            JUMP = JUMP - 2;
        }

        if(JUMP < 2 && rb2d.velocity.y == 0)
        {
            JUMP = JUMP + 1;
        }
	}
}
